const { BeforeAll, After, Status } = require('cucumber');
import { browser } from 'protractor';
import { Before } from 'cucumber';

BeforeAll({timeout: 30 * 1000}, async () => {});

After(async function() {
    await browser.executeScript('return window.localStorage.clear();');
});

After(async function(scenario) {
    if (scenario.result.status === Status.FAILED) {
        // screenShot is a base-64 encoded PNG
         const screenShot = await browser.takeScreenshot();
         this.attach(screenShot, 'image/png');
    }
});

/*
AfterAll({timeout: 100 * 1000}, async () => {
    await browser.quit();
});
*/
