import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TakeawayComponent} from './takeaway/takeaway.component';
import {TakeInComponent} from './take-in/take-in.component';
import {UserAuthGuard} from '../core/routeGuards/user-auth-guard.service';
import {OrderDetailComponent} from './order-detail/order-detail.component';

const routes: Routes = [
  {
    path: 'take-in',
    component: TakeInComponent,
    canActivate: [UserAuthGuard]
  },
  {
    path: 'takeaway',
    component: TakeawayComponent,
    canActivate: [UserAuthGuard]
  },
  {
    path: 'orderDetail',
    component: OrderDetailComponent,
    canActivate: [UserAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderRoutingModule { }
