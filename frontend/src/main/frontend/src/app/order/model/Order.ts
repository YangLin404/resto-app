import {OrderLine} from './OrderLine';
import {isNullOrUndefined} from 'util';

export class Order {
  id: string;
  date: Date;
  time: Date;
  status: Status;
  payMethod: PayMethod;
  orderType: OrderType;
  orderLines: OrderLine[];
  tableNr: number;

  constructor(id, date, time, status, payMethod, orderType, orderLines, tableNr=null) {
    this.id = id;
    this.date = date;
    this.time = time;
    this.status = status;
    this.payMethod = payMethod;
    this.orderType = orderType;
    this.orderLines = orderLines;
    this.tableNr = tableNr;
  }

  isPaid(): boolean {
    return this.status === Status.PAID;
  }

  getTableNr() {
    return this.tableNr;
  }

  public static calPrice(o: Order) {
    let price = 0;
    if (!(isNullOrUndefined(o.orderLines))) {
      o.orderLines.forEach(ol => {
        price += OrderLine.calPrice(ol);
      })
    }
    return price;
  }
}

export enum Status {
  ACTIVE = "ACTIVE", PAID = "PAID", DELETED = "DELETED"
}

export enum PayMethod {
  Cash = "Cash", Card = "Card", None = "None"
}

export enum OrderType {
  TakeIn = "TakeIn", TakeOut = "TakeOut"
}
