import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TakeawayComponent} from './takeaway.component';
import {UserAuthGuard} from '../../core/routeGuards/user-auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: TakeawayComponent,
    canActivate: [UserAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TakeawayRoutingModule { }
