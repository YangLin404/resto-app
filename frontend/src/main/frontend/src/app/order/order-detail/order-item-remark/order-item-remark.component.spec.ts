import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderItemRemarkComponent } from './order-item-remark.component';

describe('OrderItemRemarkComponent', () => {
  let component: OrderItemRemarkComponent;
  let fixture: ComponentFixture<OrderItemRemarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderItemRemarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderItemRemarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
