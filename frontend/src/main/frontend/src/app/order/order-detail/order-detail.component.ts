import {Component, OnInit, ViewChild} from '@angular/core';
import {TakeInService} from '../services/take-in.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Order, OrderType, PayMethod} from '../model/Order';
import {TakeInOrder} from '../model/TakeInOrder';
import {Product} from '../../product/model/Product';
import {OrderLine} from '../model/OrderLine';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import {AlertService} from '../../core/alert/alert.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PaymentComponent} from './payment/payment.component';
import {ConfirmWindowComponent} from '../../shared/confirm-window/confirm-window.component';
import {OrderItemRemarkComponent} from './order-item-remark/order-item-remark.component';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {

  rows = [];
  selected = [];
  loadingIndicator = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  private id: string;
  private orderType: OrderType;
  order: Order;
  orderPrice: number;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private _modalService: NgbModal,
              private _takeInService: TakeInService,
              private _alertService: AlertService) {
    this.route.params.subscribe(p => {
      this.id = p['id'];
      this.orderType = p['orderType'];
    })
  }

  ngOnInit() {
    if (this.orderType === OrderType.TakeIn) {
      this.fetchTakeInOrder();
    }
  }

  addOrderLineToOrder(p: Product) {
    this.loadingIndicator = true;
    const product = Product.makeCopy(p);
    const newOrderLine = OrderLine.createWithProduct(product);
    if (this.orderType === OrderType.TakeIn) {
      this._takeInService.addOrderLineToOrder(this.order.id, newOrderLine)
        .subscribe(o=> {
          this.refreshOrder(o);
        })
    }
  }

  removeOrderLineFromOrder(ol: OrderLine) {
    this.loadingIndicator = true;
    if  (this.orderType === OrderType.TakeIn) {
      this._takeInService.removeOrderLineFromOrder(this.order.id, ol.product.id)
        .subscribe(o=> {
          this.refreshOrder(o);
        })
    }
  }

  openRemoveOrderConfirmWindow() {
    const modalRef = this._modalService.open(ConfirmWindowComponent);
    modalRef.componentInstance.message = 'are you sure want to delete order of table ' + this.order.getTableNr();
    modalRef.result.then(result => {
      if (result) {
        this.removeOrder();
      }
    }, error => {} );
  }

  removeOrder() {
    if (this.orderType === OrderType.TakeIn) {
      this._takeInService.removeOrder(this.order.id)
        .subscribe( response => {
            this.router.navigate(['/order/take-in'])
              .then(response => {
                this._alertService.success('Remove order of table ' + this.order.getTableNr() + ' is succeeded.');
              }
              )
          }
        )
    }
  }

  calOrderLinePrice(orderLine: OrderLine) {
    return OrderLine.calPrice(orderLine);
  }

  calOrderPrice() {
    return Order.calPrice(this.order);
  }

  openPayOrder() {
    const paymentModal = this._modalService.open(PaymentComponent);
    paymentModal.componentInstance.price = this.orderPrice;
    paymentModal.result.then(payMethod => {
      if (payMethod) {
        this._takeInService.updatePayMethod(this.order.id, payMethod)
          .subscribe(response => {
            this.handlePayment(payMethod);
          })
      }
    }, error => {})
  }

  openItemRemark(orderLine: OrderLine) {
    const orderItemRemarkModal = this._modalService.open(OrderItemRemarkComponent);
    orderItemRemarkModal.componentInstance.orderLine = orderLine;
  }

  onItemSelected({ selected }) {
    const selectedProduct = this.selected[0];
    this.selected = [];
    this.openItemRemark(selectedProduct);
  }

  // needed, otherwise it cause ExpressionChangedAfterItHasBeenCheckedError when select row
  onActivate(event: any) {
    if (event.type === 'click') {
      event.cellElement.blur();
    }
  }

  private handlePayment(payMethod) {
    this.order.payMethod = payMethod;
    if (payMethod === PayMethod.Card || payMethod === PayMethod.Cash) {
      this.router.navigate(['/order/take-in'])
        .then(response => {
            this._alertService.success('table ' + this.order.getTableNr() + ' successfully paid with ' + payMethod);
          }
        )
    }
  }

  private fetchTakeInOrder() {
    this.loadingIndicator = true;
    this._takeInService.getOrderById(this.id).subscribe(t => {
      this.refreshOrder(t);
    })
  }

  private refreshOrder(o: Order) {
    this.order = new TakeInOrder(o);
    this.orderPrice = this.calOrderPrice();
    this.loadingIndicator = false;
  }
}
