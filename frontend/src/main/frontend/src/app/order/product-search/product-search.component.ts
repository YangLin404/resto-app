import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ProductService} from '../../shared/services/product.service';
import {Product} from '../../product/model/Product';
import {Observable} from 'rxjs/Observable';
import {NgbTypeaheadSelectItemEvent} from '@ng-bootstrap/ng-bootstrap';
import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';

@Component({
  selector: 'app-order-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.css']
})
export class ProductSearchComponent implements OnInit {

  products: Product[];

  model: any;

  @Output() onItemSelected = new EventEmitter<Product>();

  constructor(private _productService: ProductService) { }

  ngOnInit() {
    this._productService.getAllProducts().subscribe(p => {
      this.products = p;
    })
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(100),
      distinctUntilChanged(),
      map(term => term.length < 1 ? []
        : this.products.filter(item => this.doesItMatch(item, term)).slice(0, 15))
    );

  formatter = (x: {quicklink: string}) => '';

  itemSelected(selectedItem: NgbTypeaheadSelectItemEvent): void {
    const item: Product = selectedItem.item;
    // send call to parent
    this.onItemSelected.emit(item);
  }

  private doesItMatch(item: Product, term: string): boolean {
    term = term.toLowerCase();
    return (item.quicklink.toLowerCase().indexOf(term) > -1) || (item.name.toLowerCase().indexOf(term) > -1);
  }

}
