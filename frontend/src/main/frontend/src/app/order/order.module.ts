import { NgModule } from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {TakeInModule} from './take-in/take-in.module';
import {OrderRoutingModule} from './order-routing.module';
import {LoadingModule} from 'ngx-loading';
import {TakeawayModule} from './takeaway/takeaway.module';
import {OrderDetailComponent} from './order-detail/order-detail.component';
import { ProductSearchComponent } from './product-search/product-search.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { PaymentComponent } from './order-detail/payment/payment.component';
import {OrderService} from './services/order.service';
import { OrderItemRemarkComponent } from './order-detail/order-item-remark/order-item-remark.component';


@NgModule({
  imports: [
    SharedModule,
    LoadingModule,
    OrderRoutingModule,
    TakeInModule,
    TakeawayModule,
    NgbModule,
    NgxDatatableModule
  ],

  declarations: [OrderDetailComponent, ProductSearchComponent, PaymentComponent, OrderItemRemarkComponent],
  entryComponents: [PaymentComponent, OrderItemRemarkComponent],
  providers: [OrderService]
})
export class OrderModule { }
