import { TestBed, inject } from '@angular/core/testing';

import { TakeInService } from './take-in.service';

describe('TakeInService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TakeInService]
    });
  });

  it('should be created', inject([TakeInService], (service: TakeInService) => {
    expect(service).toBeTruthy();
  }));
});
