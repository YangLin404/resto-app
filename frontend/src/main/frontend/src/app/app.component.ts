import {Component, OnInit} from '@angular/core';
import {environment} from '../environments/environment';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'nl', 'cn']);
    translate.setDefaultLang('nl');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|nl|cn/) ? browserLang : 'nl');
  }

  ngOnInit(): void {
  }
}
