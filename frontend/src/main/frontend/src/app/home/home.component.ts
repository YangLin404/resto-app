import { Component, OnInit } from '@angular/core';
import {AuthFacadeService} from '../core/services/authServices/auth-facade.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _authFacadeService: AuthFacadeService) { }

  ngOnInit() {
    this._authFacadeService.isLoggedIn();
  }

}
