import { NgModule } from '@angular/core';
import {NgbModule, NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';

import { ProductRoutingModule } from './product-routing.module';
import { ProductListComponent } from './product-list/product-list.component';
import {ProductListResultComponent} from './product-list/prduct-list-result/product-list-result.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import {SharedModule} from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

import { LoadingModule } from 'ngx-loading';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ProductComponent } from './product.component';
import { SideDishListComponent } from './side-dish-list/side-dish-list.component';
import { SideDishDetailComponent } from './side-dish-detail/side-dish-detail.component';


@NgModule({
  imports: [
    SharedModule,
    NgbModule,
    NgbTabsetModule,
    ProductRoutingModule,
    ReactiveFormsModule,
    LoadingModule,
    NgxDatatableModule
  ],
  declarations: [
    ProductComponent,
    ProductListComponent,
    ProductListResultComponent,
    ProductDetailComponent,
    ProductComponent,
    SideDishListComponent,
    SideDishDetailComponent],

  entryComponents: [ProductDetailComponent, SideDishDetailComponent]
})
export class ProductModule { }
