import { Component, OnInit, ViewChild, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Product } from '../../model/Product';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmWindowComponent } from '../../../shared/confirm-window/confirm-window.component';
import { ProductService } from '../../../shared/services/product.service';
import { ProductDetailComponent } from '../../product-detail/product-detail.component';
import { AlertService } from '../../../core/alert/alert.service';

import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-product-list-result',
  templateUrl: './product-list-result.component.html',
  styleUrls: ['./product-list-result.component.scss']
})
export class ProductListResultComponent implements OnInit {

  /**
   * TODO: todo list for NGX datatable:
   *  - Add button for new item
   */

  /**
   * NGX Datatable
   * https://swimlane.gitbook.io/ngx-datatable
   */
  rows = [];

  temp = [];

  columns = [
    { prop: 'quicklink' },
    { name: 'naam' },
    { name: 'type' },
    { name: 'prijs' },
    { name: 'buttons' },
    { name: 'Actions' }
  ];
  @ViewChild(DatatableComponent) table: DatatableComponent;

  productResult: Product[];
  productTypes: String[];

  showData = true;

  public loading = false;

  constructor(private _modalService: NgbModal, private _productService: ProductService,
    private _alertService: AlertService) { }

  ngOnInit() {
    this.getAllProducts();
    this.getAllProductTypes();
  }

  private deleteProduct(productToDelete: Product) {
    this._productService.removeProduct(productToDelete.id)
      .subscribe(resp => {
        this.getAllProducts();
        this._alertService.success('Verwijderen van ' + productToDelete.name + ' is gelukt.');
      },
        error => {
          this._alertService.error('Verwijderen van product is mislukt, probeer nogmaals.');
        });

  }

  /**
   * Filter the Table.ts
   * @param filterValue The value to be filtered on
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(filterValue) !== -1 ||
        d.type.toLowerCase().indexOf(filterValue) !== -1 ||
        !filterValue;
    });

    // update the rows
    this.rows = [...temp];
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;

  }

  public openDeleteProductConfirm(message: string, productToDelete: Product) {
    const modalRef = this._modalService.open(ConfirmWindowComponent);
    modalRef.componentInstance.message = message;
    modalRef.result.then(result => {
      if (result) {
        this.deleteProduct(productToDelete);
      }
    });
  }

  openProductDetail(product: Product) {
    const productDetailModal = this._modalService.open(ProductDetailComponent);
    productDetailModal.componentInstance.product = product;
    productDetailModal.result.then(result => {
      if (result instanceof Product) {
        this.getAllProducts();
      }
    });
  }

  getAllProducts() {
    // TODO: finally => close the loader after errors too
    this.loading = true;
    this._productService.getAllProducts()
      .subscribe((orders) => {
        this.productResult = orders;
        this.temp = [...orders];
        this.rows = orders;
        this.loading = false;
      });
  }

  getAllProductTypes(): any {
    this._productService.getAllProductTypes()
      .subscribe((types: string[]) => {
        this.productTypes = types;
      });
  }

  addNewProduct() {
    const productDetailModal = this._modalService.open(ProductDetailComponent);
    productDetailModal.componentInstance.product = Product.newProduct();
    productDetailModal.result.then(result => {
      if (result instanceof Product) {
        this.getAllProducts();
        this._alertService.success('Toevoegen van product ' + result.name + ' is gelukt.');
      }
    });
  }

  /**
   * Get the TypeClass to style the different types
   */
  getTypeClass(type: string) {
    switch (type) {
      case 'Drink':
        return 'badge-secondary';
      case 'Soup':
        return 'badge-primary';
      case 'MainDishe':
        return 'badge-success';
      case 'Entree':
        return 'badge-warning';
      case 'Dessert':
        return 'badge-info';
      case 'Supplement':
        return 'badge-danger';
      case 'Menu':
        return 'badge-dark';
      default:
        return '';
    }
  }
}
