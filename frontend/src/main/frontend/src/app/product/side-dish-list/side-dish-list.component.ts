import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AlertService} from '../../core/alert/alert.service';
import {SideDishService} from '../../shared/services/side-dish.service';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {SideDish} from '../model/SideDish';
import {SideDishDetailComponent} from '../side-dish-detail/side-dish-detail.component';

@Component({
  selector: 'app-side-dish-list',
  templateUrl: './side-dish-list.component.html',
  styleUrls: ['./side-dish-list.component.scss']
})
export class SideDishListComponent implements OnInit {

  loadingIndicator = true;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  sideDishes: SideDish[];

  constructor(private _modalService: NgbModal, private _sideDishService: SideDishService,
              private _alertService: AlertService) { }


  ngOnInit() {
    this.getAllSideDishes();
  }

  private getAllSideDishes() {
    this.loadingIndicator = true;
    this._sideDishService.getAll().subscribe(t => {
      this.sideDishes = t;
      this.loadingIndicator = false;
    }, error => {
      this.loadingIndicator = false;
    })
  }

  addNewSideDish() {
    const detailModal = this._modalService.open(SideDishDetailComponent);
    detailModal.componentInstance.sideDish = SideDish.newSideDish();
    detailModal.result.then(result => {
      if (result instanceof SideDish) {
        this.getAllSideDishes();
        this._alertService.success('Toevoegen van side dish ' + result.name + ' is gelukt.');
      }
    })
  }

  deleteSideDish(id: string) {
    this.loadingIndicator = true;
    this._sideDishService.deleteSideDish(id)
      .subscribe(response => {
        this.getAllSideDishes();
      }, error => {
        this.loadingIndicator = false;
      })
  }
}
