import {Component, Input, OnInit} from '@angular/core';
import {SideDish} from '../model/SideDish';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AlertService} from '../../core/alert/alert.service';
import {SideDishService} from '../../shared/services/side-dish.service';

@Component({
  selector: 'app-side-dish-detail',
  templateUrl: './side-dish-detail.component.html',
  styleUrls: ['./side-dish-detail.component.css']
})
export class SideDishDetailComponent implements OnInit {

  @Input()
  public sideDish:SideDish;

  form: FormGroup;

  constructor(public activeModal: NgbActiveModal,
              private _sideDishService: SideDishService,
              private _alertService: AlertService,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name:  [this.sideDish.name],
      price: [this.sideDish.price]
    });
  }

  addNewSideDish() {
    const sideDishToAdd = new SideDish(null, this.form.controls['name'].value, this.form.controls['price'].value);
    this._sideDishService.addNewSideDish(sideDishToAdd)
      .subscribe((result: SideDish) => {
        this.activeModal.close(SideDish.create(result));
      }, error => {
        this.activeModal.close(null);
      } )
  }

}
