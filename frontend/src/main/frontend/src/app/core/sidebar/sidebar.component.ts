import {Component, OnInit} from '@angular/core';
import {AuthFacadeService} from '../services/authServices/auth-facade.service';

@Component({
  selector: 'app-core-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {
  username: string;
  sidenavWidth = 5;
  constructor(private _authFacedeService: AuthFacadeService) {
  }

  ngOnInit(): void {
  }

  isLoggedIn(): boolean {
    if (this._authFacedeService.isLoggedIn()) {
      this.username = this._authFacedeService.getCurrentUsername();
      return true;
    }
    return false;
  }

  isAdmin(): boolean {
    return this._authFacedeService.isAdmin();
  }

  getUsername() {
    return this.username;
  }

  /*
  openMenu() {
    this.sidenavWidth = 15;
    this.menuOpened = true;
  }

  closeMenu() {
    this.sidenavWidth = 5;
    this.menuOpened = false;
  }
  */
}
