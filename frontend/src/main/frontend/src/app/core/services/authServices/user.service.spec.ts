import { TestBed} from '@angular/core/testing';

import { UserService } from './user.service';
import {User} from '../../models/User';

const USERNAME_ADMIN = 'admin';

describe('UserService', () => {

  const TOKEN_ADMIN_NOT_EXPIRED = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbi' +
    'IsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHki' +
    'OiJST0xFX0FETUlOIn1dLCJleHAiOjM1MjQ4MjAxMzN9.6jrYOhHq' +
    '1we7nDUjhm8CaKlDeZgRJ4RhiXywtcrAX0JuZNBdrFTIcHdDlmQXuDd3943cwSDBQPA9w-7_Mf8OmA';

  let service: UserService;

  beforeEach(() => {
    // authService = jasmine.createSpyObj('AuthService', ['getToken']);

    TestBed.configureTestingModule({
      providers: [
        UserService
      ]
    });

    service = TestBed.get(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('createCurrentUser', () => {
    it('should create current User', () => {
      const userExpected = aAdminUser();

      service.createCurrentUser(TOKEN_ADMIN_NOT_EXPIRED);

      expect(service.getCurrentUser()).toEqual(userExpected);
    });
  });

  describe('getCurrentUsername', function () {
    it('should get current username', function () {
      service.currentUser = aAdminUser();

      expect(service.getCurrentUsername()).toEqual(USERNAME_ADMIN);
    });
  });

  describe('isAdmin', function () {
    it('should return true', function () {
      service.currentUser = aAdminUser();

      expect(service.isAdmin()).toEqual(true);
    });

    it('should return false', function () {
      service.currentUser = aUser();

      expect(service.isAdmin()).toEqual(false);
    });
  });

  describe('currentUserExist', function () {
    it('should return true', function () {
      service.currentUser = aAdminUser();

      expect(service.currentUserExist()).toEqual(true);
    });

    it('should return false', function () {

      expect(service.currentUserExist()).toEqual(false);
    });
  });

  describe('logoutCurrentUser', function () {
    it('should logout current user', function () {
      service.currentUser = aAdminUser();

      service.logoutCurrentUser();

      expect(service.currentUser).toBeNull();
      expect(service.currentUserExist()).toEqual(false);
    });
  });

  describe('getCurrentUser', function () {
    it('should return current user', function () {
      const userExcepted = aAdminUser();
      service.currentUser = userExcepted;

      expect(service.getCurrentUser()).toEqual(userExcepted);
    });
  });
});

function aAdminUser(): User {
  const user = new User();
  user.username = USERNAME_ADMIN;
  user.role = 'ROLE_ADMIN';
  return user;
}

function aUser(): User {
  const user = aAdminUser();
  user.role = 'ROLE_USER';
  return user;
}
