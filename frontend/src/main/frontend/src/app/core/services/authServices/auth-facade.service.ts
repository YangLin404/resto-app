import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';
import {UserService} from './user.service';
import {AlertService} from '../../alert/alert.service';

@Injectable()
export class AuthFacadeService {

  constructor(private _authService: AuthService, private _userService: UserService) {
  }

  login(loginData) {
    return this._authService.obtainAccessToken(loginData);
  }

  logout() {
    this._userService.logoutCurrentUser();
    this._authService.clearToken();
  }

  transparentLogin() {
    if (this.canDoTransparentLogin()) {
      this._userService.createCurrentUser(this._authService.getToken());
    }
  }

  canDoTransparentLogin() {
    return this._authService.isAuthenticated() &&
      !this._authService.isTokenExpired() &&
      !this._userService.currentUserExist();
  }

  getCurrentUsername() {
    return this._userService.getCurrentUsername();
  }

  isAdmin() {
    if (!this.isLoggedIn()) {
      this.transparentLogin();
    }
    return this._authService.isAuthenticated() && this._userService.isAdmin();
  }

  isLoggedIn() {
    return this._userService.currentUserExist() && this._authService.isAuthenticated();
  }

  getToken(): string {
    return this._authService.getToken();
  }

  saveAccessToken(authToken) {
    this._authService.saveAccessToken(authToken);
  }
}
