import {TestBed} from '@angular/core/testing';
import {AuthService} from './auth.service';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';
import {AlertService} from '../../alert/alert.service';

describe('AuthService', () => {
  const ACCESS_TOKEN = 'access_token';
  const ACCESS_TOKEN_VALUE = 'token_value';
  const TOKEN_ADMIN_NOT_EXPIRED = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbi' +
                                  'IsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHki' +
                                  'OiJST0xFX0FETUlOIn1dLCJleHAiOjM1MjQ4MjAxMzN9.6jrYOhHq' +
                                  '1we7nDUjhm8CaKlDeZgRJ4RhiXywtcrAX0JuZNBdrFTIcHdDlmQXuDd3943cwSDBQPA9w-7_Mf8OmA';
  const TOKEN_ADMIN_EXPIRED = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsIm' +
                              'F1dGhvcml0aWVzIjpbeyJhdXRob3JpdHkiOiJST0xFX' +
                              '0FETUlOIn1dLCJleHAiOjE1MjQ4MjAxMzN9.peQ3EsA' +
                              'fR16PqdoLCYPDnMCK9Swism5W3uO2_2lJQWdfCEiDd3rp8PyBDkfKnrXD3feyMuhU_PLm9TqPBJgBUQ';

  const HEADER_VALUE = 'bear ' + TOKEN_ADMIN_NOT_EXPIRED;

  let authService: AuthService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let userServiceSpy: jasmine.SpyObj<UserService>;
  let alertServiceSpy: jasmine.SpyObj<AlertService>;

  beforeEach(() => {
    httpClientSpy =
      jasmine.createSpyObj('HttpClient', ['get', 'post']);
    userServiceSpy =
      jasmine.createSpyObj('UserService', [
        'isAdmin', 'createCurrentUser', 'logoutCurrentUser']);
    alertServiceSpy =
      jasmine.createSpyObj('AlertService', ['']);

    TestBed.configureTestingModule({
      providers: [
        AuthService,
        {provide: HttpClient, useValue: httpClientSpy},
        {provide: UserService, useValue: userServiceSpy},
        {provide: AlertService, useValue: alertServiceSpy}
      ]
    });

    authService = TestBed.get(AuthService);
    userServiceSpy = TestBed.get(UserService);
  });

  // mock localStorage
  beforeEach(() => {
    let store = {};

    spyOn(localStorage, 'getItem').and.callFake( (key: string): String => {
      return store[key] || null;
    });
    spyOn(localStorage, 'removeItem').and.callFake((key: string): void =>  {
      delete store[key];
    });
    spyOn(localStorage, 'setItem').and.callFake((key: string, value: string): string =>  {
      return store[key] = <string>value;
    });
    spyOn(localStorage, 'clear').and.callFake(() =>  {
      store = {};
    });
  });

  it('should create the service', (() => {
    expect(authService).toBeTruthy();
  }));

  describe('#isAuthenticated', () => {
    it('should return is authenticated', function () {
      localStorage.setItem(ACCESS_TOKEN, ACCESS_TOKEN_VALUE);
      expect(authService.isAuthenticated()).toBe(true);
    });

    it('should return is not authenticated', function () {
      expect(authService.isAuthenticated()).toBe(false);
    });
  });

  describe('#logout', function () {
    it('should remove token in localStorage when log out', function () {
      localStorage.setItem(ACCESS_TOKEN, ACCESS_TOKEN_VALUE);
      authService.clearToken();
      expect(localStorage.removeItem).toHaveBeenCalled();
      expect(localStorage.getItem(ACCESS_TOKEN)).toBeNull();
    });
  });

  describe('#getToken', () => {
    it('should return token', function () {
      localStorage.setItem(ACCESS_TOKEN, ACCESS_TOKEN_VALUE);
      expect(authService.getToken()).toEqual(ACCESS_TOKEN_VALUE);
    });

    it('should not return token', function () {
      expect(authService.getToken()).toBeNull();
    });
  });

  describe('#saveAccessToken', function () {
    it('should save access token to localstorage', function () {
      authService.saveAccessToken(HEADER_VALUE);
      expect(localStorage.getItem(ACCESS_TOKEN)).toEqual(TOKEN_ADMIN_NOT_EXPIRED);
      expect(localStorage.setItem).toHaveBeenCalled();
    });
  });

  describe('#isTokenExpired', function () {
    it('should return true', function () {
      localStorage.setItem(ACCESS_TOKEN, TOKEN_ADMIN_EXPIRED);

      expect(authService.isTokenExpired()).toEqual(true);
    });

    it('should return false', function () {
      localStorage.setItem(ACCESS_TOKEN, TOKEN_ADMIN_NOT_EXPIRED);

      expect(authService.isTokenExpired()).toEqual(false);
    });
  });

  afterEach(() => {
    localStorage.clear();
  });
});
