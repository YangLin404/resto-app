import { Injectable } from '@angular/core';
import { User } from '../../models/User';
import * as jwt_decode from 'jwt-decode';
import {AppConfig} from '../../../shared/configs/AppConfig';

@Injectable()
export class UserService {

  currentUser: User;

  constructor() {}

  getCurrentUsername(): string {
    return this.currentUser.username;
  }

  createCurrentUser(authToken: string) {
    const tokenInfo = jwt_decode(authToken);
    this.currentUser = new User();
    this.currentUser.username = tokenInfo.sub;
    this.currentUser.role = tokenInfo.authorities[0].authority;
  }

  isAdmin(): boolean {
    return this.currentUserExist() && this.currentUser.role === AppConfig.ROLE_ADMIN;
  }

  currentUserExist(): boolean {
    return this.currentUser !== null && this.currentUser !== undefined;
  }

  logoutCurrentUser() {
    this.currentUser = null;
  }

  getCurrentUser(): User {
    return this.currentUser;
  }
}
