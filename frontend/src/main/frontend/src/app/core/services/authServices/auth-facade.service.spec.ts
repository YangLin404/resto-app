import { TestBed } from '@angular/core/testing';

import { AuthFacadeService } from './auth-facade.service';
import {AuthService} from './auth.service';
import {UserService} from './user.service';
import {AlertService} from '../../alert/alert.service';

describe('AuthFacadeService', () => {

  const USERNAME_ADMIN = 'admin';
  const ACCESS_TOKEN_VALUE = 'token_value';

  let authServiceSpy: jasmine.SpyObj<AuthService>;
  let userServiceSpy: jasmine.SpyObj<UserService>;
  let alertServiceSpy: jasmine.SpyObj<AlertService>;
  let service: AuthFacadeService;

  beforeEach(() => {
     authServiceSpy = jasmine.createSpyObj(
       'AuthService', [
         'clearToken', 'isTokenExpired', 'isAuthenticated', 'getToken']);
     userServiceSpy = jasmine.createSpyObj(
       'UserService', [
         'logoutCurrentUser', 'currentUserExist', 'createCurrentUser', 'getCurrentUsername', 'isAdmin']);

     alertServiceSpy = jasmine.createSpyObj(
       'AlertService', ['']
     );

    TestBed.configureTestingModule({
      providers: [
        AuthFacadeService,
        {provide: AuthService, useValue: authServiceSpy},
        {provide: UserService, useValue: userServiceSpy},
        {provide: AlertService, useValue: alertServiceSpy}
      ]
    });

    service = TestBed.get(AuthFacadeService);
    authServiceSpy = TestBed.get(AuthService);
    userServiceSpy = TestBed.get(UserService);
    alertServiceSpy = TestBed.get(AlertService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#logout', function () {
    it('should log out current user', function () {
      userServiceSpy.logoutCurrentUser.and.callThrough();
      service.logout();

      expect(authServiceSpy.clearToken).toHaveBeenCalled();
      expect(userServiceSpy.logoutCurrentUser).toHaveBeenCalled();
    });
  });

  describe('#canDoTransparentLogin', function () {
    it('should return true', function () {
      userServiceSpy.currentUserExist.and.returnValue(false);
      authServiceSpy.isAuthenticated.and.returnValue(true);
      authServiceSpy.isTokenExpired.and.returnValue(false);

      expect(service.canDoTransparentLogin()).toEqual(true);
      expect(userServiceSpy.currentUserExist).toHaveBeenCalled();
      expect(authServiceSpy.isAuthenticated).toHaveBeenCalled();
      expect(authServiceSpy.isTokenExpired).toHaveBeenCalled();
    });

    it('should return false when current user exist', function () {
      userServiceSpy.currentUserExist.and.returnValue(true);
      authServiceSpy.isAuthenticated.and.returnValue(true);
      authServiceSpy.isTokenExpired.and.returnValue(false);

      expect(service.canDoTransparentLogin()).toEqual(false);
      expect(userServiceSpy.currentUserExist).toHaveBeenCalled();
      expect(authServiceSpy.isAuthenticated).toHaveBeenCalled();
      expect(authServiceSpy.isTokenExpired).toHaveBeenCalled();
    });

    it('should return false when token is expired', function () {
      userServiceSpy.currentUserExist.and.returnValue(false);
      authServiceSpy.isAuthenticated.and.returnValue(true);
      authServiceSpy.isTokenExpired.and.returnValue(true);

      expect(service.canDoTransparentLogin()).toEqual(false);
      expect(authServiceSpy.isAuthenticated).toHaveBeenCalled();
      expect(authServiceSpy.isTokenExpired).toHaveBeenCalled();
    });
  });

  describe('#transparentLogin', function () {
    it('should transparent login user', function () {
      userServiceSpy.currentUserExist.and.returnValue(false);
      authServiceSpy.isAuthenticated.and.returnValue(true);
      authServiceSpy.isTokenExpired.and.returnValue(false);

      service.transparentLogin();

      expect(userServiceSpy.createCurrentUser).toHaveBeenCalled();
    });

    it('should not transparent login user', function () {
      userServiceSpy.currentUserExist.and.returnValue(true);
      authServiceSpy.isAuthenticated.and.returnValue(true);
      authServiceSpy.isTokenExpired.and.returnValue(false);

      service.transparentLogin();

      expect(userServiceSpy.createCurrentUser).toHaveBeenCalledTimes(0);
    });
  });

  describe('#getCurrentUsername', function () {
    it('should return username admin', function () {
      userServiceSpy.getCurrentUsername.and.returnValue(USERNAME_ADMIN);

      expect(service.getCurrentUsername()).toEqual(USERNAME_ADMIN);
    });
  });

  describe('#isAdmin', function () {
    it('should return true', function () {
      userServiceSpy.currentUserExist.and.returnValue(true);
      userServiceSpy.isAdmin.and.returnValue(true);
      authServiceSpy.isAuthenticated.and.returnValue(true);

      expect(service.isAdmin()).toEqual(true);
      expect(userServiceSpy.isAdmin).toHaveBeenCalled();

    });

    it('should return false', function () {
      userServiceSpy.currentUserExist.and.returnValue(true);
      userServiceSpy.isAdmin.and.returnValue(false);
      authServiceSpy.isAuthenticated.and.returnValue(true);

      expect(service.isAdmin()).toEqual(false);
      expect(userServiceSpy.isAdmin).toHaveBeenCalled();

    });
  });

  describe('#isLoggedIn', function () {
    it('should return true', function () {
      authServiceSpy.isAuthenticated.and.returnValue(true);
      userServiceSpy.currentUserExist.and.returnValue(true);

      expect(service.isLoggedIn()).toEqual(true);
    });

    it('should return false', function () {
      authServiceSpy.isAuthenticated.and.returnValue(true);
      userServiceSpy.currentUserExist.and.returnValue(false);

      expect(service.isLoggedIn()).toEqual(false);
    });
  });

  describe('#getToken', function () {
    it('should return token', function () {
      authServiceSpy.getToken.and.returnValue(ACCESS_TOKEN_VALUE);

      expect(service.getToken()).toEqual(ACCESS_TOKEN_VALUE);
    });
  });
});
