import { Component, OnInit } from '@angular/core';
import { AuthFacadeService } from '../services/authServices/auth-facade.service';
import { UserService } from '../services/authServices/user.service';
import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-core-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  public loginData = { username: '', password: '' };

  public loading = false;

  constructor(private _authFacadeService: AuthFacadeService,
    private _userService: UserService,
    private _alertService: AlertService) { }

  ngOnInit(): void { }

  login() {
    this.loading = true;
    this._authFacadeService.login(this.loginData).subscribe(
      resp => {
        const authToken = resp.headers.get('Authorization');
        this._authFacadeService.saveAccessToken(authToken);
        this._userService.createCurrentUser(authToken);
        this.loading = false;
      },
      error => {
        if (error.status === 401) {
          this._alertService.error('Gebruikersnaam en wachtwoord komen niet overeen.');
        }
        this.loading = false;
      });
  }

  logout() {
    this._authFacadeService.logout();
  }

  isLoggedIn(): boolean {
    return this._authFacadeService.isLoggedIn();
  }

  public getLoggedInUsername() {
    return this._authFacadeService.getCurrentUsername();
  }
}
