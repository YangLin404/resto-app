import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {LoginComponent} from './login.component';
import {FormsModule} from '@angular/forms';
import {AuthFacadeService} from '../services/authServices/auth-facade.service';


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authFacadeServiceSpy: jasmine.SpyObj<AuthFacadeService>;

  beforeEach(async(() => {
    authFacadeServiceSpy = jasmine.createSpyObj(
        'AuthFacadeService', ['isLoggedIn', 'logout', 'getCurrentUsername']);

    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [ LoginComponent ],
      providers: [ {provide: AuthFacadeService, useValue: authFacadeServiceSpy} ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#user is not logged in', function () {
    beforeEach(() => {
      authFacadeServiceSpy.isLoggedIn.and.returnValue(false);
    });
    it('should render login form when user is not logged in', function () {
      const loginElement: HTMLElement = fixture.nativeElement;
      const loginFormEl = loginElement.querySelector('.form-login');

      component.ngOnInit();

      expect(loginFormEl).toBeDefined();
    });
  });
});
