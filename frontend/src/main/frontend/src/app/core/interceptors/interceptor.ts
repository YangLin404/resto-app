import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/materialize';
import 'rxjs/add/operator/dematerialize';
import { ProductInterceptorService } from '../services/interceptor-services/products.interceptor.service';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    constructor(private productService: ProductInterceptorService) { }

    /**
     * intercept the http requests
     * check https://github.com/cornflourblue/angular2-registration-login-example/blob/master/app/_helpers/fake-backend.ts#L28
     * for more examples of intercepts and usage
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const SIMULATOR_ENDPOINT = '/simulator';
        const PRODUCT_ENDPOINT = '/api/product';

        // wrap in delayed observable to simulate server api call
        return Observable.of(null).mergeMap(() => {
            // authenticate
            if (request.url.endsWith(SIMULATOR_ENDPOINT + '/login') && request.method === 'POST') {
              if (request.body.username === "error") {
                return Observable.throw(new HttpErrorResponse(({status: 401})));
              } else {
                const header = new HttpHeaders();
                const other_header = header.append('Authorization', 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbi' +
                  'IsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHki' +
                  'OiJST0xFX0FETUlOIn1dLCJleHAiOjM1MjQ4MjAxMzN9.6jrYOhHq' +
                  '1we7nDUjhm8CaKlDeZgRJ4RhiXywtcrAX0JuZNBdrFTIcHdDlmQXuDd3943cwSDBQPA9w-7_Mf8OmA');

                return Observable.of(new HttpResponse({status: 200, body: null, headers: other_header}));
              }
            }

            // Get all products
            if (request.url.endsWith(SIMULATOR_ENDPOINT + PRODUCT_ENDPOINT) && request.method === 'GET') {

                const products = this.productService.getAllproducts();

                return Observable.of(new HttpResponse({ status: 200, body: products}));
            }

            // Delete a product
            if (request.url.match(/\/simulator\/api\/product\/\d+$/) && request.method === 'DELETE') {

                // find user by id in users array
                const urlParts = request.url.split('/');
                const id = parseInt(urlParts[urlParts.length - 1], 10);
                const isProductDeleted = this.productService.deleteProduct(id);
                if (isProductDeleted.length > 0)  {
                    return Observable.of(new HttpResponse({ status: 200, body: 'Product is deleted'}));
                } else {
                    throw new Error('Error while deleting product');
                }
            }

            // Get all product types
            if (request.url.endsWith(SIMULATOR_ENDPOINT + PRODUCT_ENDPOINT + '/productType') && request.method === 'GET') {

                const productTypes = this.productService.getAllProductTypes();

                return Observable.of(new HttpResponse({ status: 200, body: productTypes}));
            }

            // Create new product
            if (request.url.endsWith(SIMULATOR_ENDPOINT + PRODUCT_ENDPOINT) && request.method === 'POST') {

                const product = request.body;
                const updatedProduct = this.productService.createOrUpdateProduct(product);

                return Observable.of(new HttpResponse({ status: 200, body: updatedProduct}));
            }

            // pass through any requests not handled above
            return next.handle(request);
        })

    // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
    .materialize()
    .delay(500)
    .dematerialize();
    }
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
