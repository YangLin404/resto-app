
import {NgModule} from '@angular/core';
import {LoginComponent} from './login/login.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {AuthService} from './services/authServices/auth.service';
import {UserAuthGuard} from './routeGuards/user-auth-guard.service';
import {CommonModule} from '@angular/common';
import {AdminRoleGuardService} from './routeGuards/admin-role-guard.service';
import { ProductInterceptorService } from './services/interceptor-services/products.interceptor.service';
import { MatSidenavModule, MatListModule, MatIconModule } from '@angular/material';
import {UserService} from './services/authServices/user.service';
import {AuthFacadeService} from './services/authServices/auth-facade.service';
import {AlertComponent} from './alert/alert.component';
import {AlertService} from './alert/alert.service';
import {SharedModule} from '../shared/shared.module';
import { LoadingModule } from 'ngx-loading';

const materialModules = [
  MatSidenavModule,
  MatListModule,
  MatIconModule
];

@NgModule({
  declarations: [
    LoginComponent,
    SidebarComponent,
    AlertComponent
  ],
  entryComponents: [
  ],
  imports: [
    NgbModule,
    FormsModule,
    CommonModule,
    RouterModule,
    [...materialModules],
    SharedModule,
    LoadingModule
  ],
  exports: [
    SidebarComponent,
    LoginComponent,
    AlertComponent
  ],
  providers: [AuthService, UserService, AuthFacadeService, AlertService,
    UserAuthGuard, AdminRoleGuardService,
    ProductInterceptorService]
})

export class CoreModule {}
