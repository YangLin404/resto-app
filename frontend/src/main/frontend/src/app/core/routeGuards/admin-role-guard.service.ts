import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthFacadeService} from '../services/authServices/auth-facade.service';

@Injectable()
export class AdminRoleGuardService implements CanActivate {

  constructor(private _authFacadeService: AuthFacadeService, private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const url: string = state.url;

    return this.checkAdminRole(url);
  }

  checkAdminRole(url: string): boolean {
    if (this._authFacadeService.isAdmin()) { return true; }
    this._router.navigate(['/home']);
    return false;
  }
}
