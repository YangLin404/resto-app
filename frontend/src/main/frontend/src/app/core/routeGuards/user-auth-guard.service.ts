import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthFacadeService} from '../services/authServices/auth-facade.service';

@Injectable()
export class UserAuthGuard implements CanActivate {

  constructor(private _authFacadeService: AuthFacadeService, private _router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const url: string = state.url;

    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    if (this._authFacadeService.isLoggedIn()) { return true; }
    this._router.navigate(['/login']);
    return false;
  }

}
