import { Component, OnInit } from '@angular/core';
import {Alert} from './alert';
import {AlertService} from './alert.service';

@Component({
  selector: 'app-shared-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  _alert: Alert;

  constructor(private _alertService: AlertService) { }

  ngOnInit() {
    this._alertService.getAlert().subscribe(
      (alert: Alert) => {
        if (!alert) {
          this.clearAlert()
        }
        this._alert = alert;
      });
  }

  shouldShowAlert(): boolean {
    return this._alert == null;
  }

  private clearAlert() {
    this._alert = null;
  }

}
