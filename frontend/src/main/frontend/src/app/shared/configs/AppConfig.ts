import {environment} from '../../../environments/environment';

export class AppConfig {
  private static API_ENDPOINT_INT = 'http://localhost:8090';

  // Uncomment the following to enable backend simulator
  private static API_ENDPOINT_STUB = 'http://localhost:8090/simulator';

  public static ACCESS_TOKEN = 'access_token';

  public static ROLE_ADMIN = 'ROLE_ADMIN';


  public static getApiEndpoint() {
    if (environment.stub) {
      return this.API_ENDPOINT_STUB;
    } else {
      return this.API_ENDPOINT_INT;
    }
  }
}
