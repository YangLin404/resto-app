import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ConfirmWindowComponent} from './confirm-window/confirm-window.component';
import { TranslateModule } from '@ngx-translate/core';
import {ProductService} from './services/product.service';
import {SideDishService} from './services/side-dish.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    ConfirmWindowComponent
  ],
  exports: [
      CommonModule,
      FormsModule,
      ConfirmWindowComponent,
      TranslateModule
    ],
  entryComponents: [ConfirmWindowComponent],
  providers: [ProductService, SideDishService]
})
export class SharedModule { }
