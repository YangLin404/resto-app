import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {AppConfig} from '../configs/AppConfig';
import {Product} from '../../product/model/Product';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {catchError} from 'rxjs/operators';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/finally';

@Injectable()
export class ProductService {

  private PRODUCT_ENDPOINT = AppConfig.getApiEndpoint() + '/api/product';

  constructor(private _httpClient: HttpClient) {
    this.reloadLocalProducts();
    this.fetchAllProductTypes()
      .subscribe(types => {
        localStorage.setItem('productTypes', JSON.stringify(types));
      });
  }

  private reloadLocalProducts() {
    this.fetchAllProducts()
      .subscribe(products => {
        localStorage.setItem('products', JSON.stringify(products))
      });
  }

  getAllProducts() {
    const products = JSON.parse(localStorage.getItem('products'));
    if (products) {
      return Observable.of(products);
    } else {
      return this.fetchAllProducts();
    }
  }

  getAllProductTypes(): Observable<string[]> {
    const types = JSON.parse(localStorage.getItem('productTypes'));
    if (types) {
      return Observable.of(types);
    } else {
      return this.fetchAllProductTypes();
    }
  }

  removeProduct(id: string) {
    this.clearLocalProducts();
    return this._httpClient.delete(this.PRODUCT_ENDPOINT + '/' + id)
      .finally(() => this.reloadLocalProducts())
      .pipe(
        catchError(this.handleError)
      )
  }

  updateProduct(product: Product) {
    this.clearLocalProducts();
    return this._httpClient.post(this.PRODUCT_ENDPOINT, product)
      .finally(() => this.reloadLocalProducts())
      .pipe(
        catchError(this.handleError)
      )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `message was ${error.error.message}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      error.error.message);
  }

  private clearLocalProducts() {
    localStorage.removeItem('products');
  }

  private fetchAllProducts() {
    return this._httpClient.get<Product[]>(this.PRODUCT_ENDPOINT);
  }

  private fetchAllProductTypes() {
    return this._httpClient.get<string[]>(this.PRODUCT_ENDPOINT + '/productType');
  }

}
