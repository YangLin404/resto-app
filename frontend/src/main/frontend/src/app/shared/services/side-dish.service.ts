import { Injectable } from '@angular/core';
import {AppConfig} from '../configs/AppConfig';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {SideDish} from '../../product/model/SideDish';
import {Observable} from 'rxjs/Observable';
import {catchError} from 'rxjs/operators';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';

@Injectable()
export class SideDishService {

  private SIDE_DISH_ENDPOINT = AppConfig.getApiEndpoint() + '/api/sideDish';
  private SIDE_DISH_LOCAL_STORAGE_KEY = 'sideDishes';

  constructor(private _httpClient: HttpClient) {
    this.reloadLocalSideDishes();
  }

  getAll() {
    const sideDishes = JSON.parse(localStorage.getItem(this.SIDE_DISH_LOCAL_STORAGE_KEY));
    if (sideDishes) {
      return Observable.of(sideDishes);
    } else {
      return this.fetchAll();
    }
  }

  public addNewSideDish(newSideDish: SideDish) {
    return this._httpClient.put(this.SIDE_DISH_ENDPOINT, newSideDish)
      .finally(() => this.reloadLocalSideDishes())
      .pipe(
        catchError(this.handleError)
      );
  }

  public deleteSideDish(id: string) {
    this.clearLocalSideDishes();
    const deleteUrl = this.SIDE_DISH_ENDPOINT + '/' + id;

    return this._httpClient.delete(deleteUrl)
      .finally(() => this.reloadLocalSideDishes())
      .pipe(
        catchError(this.handleError)
      )
  }

  private reloadLocalSideDishes() {
    this.clearLocalSideDishes();
    this.fetchAll()
      .subscribe(sideDishes => {
        localStorage.setItem(this.SIDE_DISH_LOCAL_STORAGE_KEY, JSON.stringify(sideDishes))
      });
  }

  private fetchAll() {
    return this._httpClient.get<SideDish[]>(this.SIDE_DISH_ENDPOINT);
  }

  private clearLocalSideDishes() {
    localStorage.removeItem(this.SIDE_DISH_LOCAL_STORAGE_KEY);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `message was ${error.error.message}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      error.error.message);
  }



}
