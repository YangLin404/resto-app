import { TestBed, inject } from '@angular/core/testing';

import { SideDishService } from './side-dish.service';

describe('SideDishService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SideDishService]
    });
  });

  it('should be created', inject([SideDishService], (service: SideDishService) => {
    expect(service).toBeTruthy();
  }));
});
