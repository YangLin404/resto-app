// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');
const jsonReports = process.cwd() + '/reports/json';
const Reporter = require('./e2e/support/reporter');

exports.config = {
  allScriptsTimeout: 11000,

  capabilities: {
    'browserName': 'chrome'
  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',

  specs: [
    './e2e/features/**/*.feature'
  ],

  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),

  // cucumber command line options
  cucumberOpts: {
    // require step definition files before executing features
    require: ['./e2e/steps/**/*.ts', './e2e/support/hooks.ts'],
    // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    tags: [],
    format: 'json:./reports/json/cucumber_report.json',
    // <boolean> fail if there are any undefined or pending steps
    strict: true,
    // <boolean> invoke formatters without executing steps
    dryRun: false,
    // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
    compiler: []
  },

  onPrepare() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
    Reporter.createDirectory(jsonReports);
  },
  onComplete: function () {
    Reporter.createHTMLReport();
  },
};