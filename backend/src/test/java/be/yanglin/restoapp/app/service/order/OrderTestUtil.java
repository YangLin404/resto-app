package be.yanglin.restoapp.app.service.order;

import be.yanglin.restoapp.app.model.order.Order;
import be.yanglin.restoapp.app.model.order.TakeInOrder;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class OrderTestUtil {

    static final LocalDate TAKE_IN_ORDER_DATE = LocalDate.of(2018,5,20);
    static final LocalTime TAKE_IN_ORDER_TIME = LocalTime.of(20,30,0);

    static List<TakeInOrder> aListOfTakeInOrders(int totalOrders) {
        List<TakeInOrder> takeInOrders = new ArrayList<>();
        for (int i=1; i<=totalOrders; i++) {
            takeInOrders.add(aNewTakeInOrder(i));
        }
        return takeInOrders;
    }

    static List<TakeInOrder> aListOfDeletedTakeInOrders(int totalOrders) {
        List<TakeInOrder> takeInOrders = new ArrayList<>();
        for (int i=1; i<=totalOrders; i++) {
            takeInOrders.add(aDeletedTakeInOrder(i));
        }
        return takeInOrders;
    }

    static TakeInOrder aNewTakeInOrder(int tableNr) {
        return TakeInOrder.builder()
                .date(TAKE_IN_ORDER_DATE)
                .time(TAKE_IN_ORDER_TIME)
                .payMethod(Order.PayMethod.None)
                .orderLines(new ArrayList<>())
                .status(Order.Status.ACTIVE)
                .tableNr(tableNr)
                .build();
    }

    static TakeInOrder aExistingTakeInOrder(int tableNr) {
        final TakeInOrder takeInOrder = aNewTakeInOrder(tableNr);
        takeInOrder.setId("Test");
        return takeInOrder;
    }

    static TakeInOrder aDeletedTakeInOrder(int tableNr) {
        final TakeInOrder takeInOrder = aExistingTakeInOrder(tableNr);
        takeInOrder.setStatus(Order.Status.DELETED);
        return takeInOrder;
    }


}
