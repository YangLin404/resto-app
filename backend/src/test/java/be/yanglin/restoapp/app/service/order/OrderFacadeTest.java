package be.yanglin.restoapp.app.service.order;

import be.yanglin.restoapp.app.exception.order.OrderNotFoundException;
import be.yanglin.restoapp.app.exception.order.TableAlreadyActiveException;
import be.yanglin.restoapp.app.model.order.Order;
import be.yanglin.restoapp.app.model.order.OrderType;
import be.yanglin.restoapp.app.model.order.TakeInOrder;
import be.yanglin.restoapp.app.repo.order.TakeInOrderRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static be.yanglin.restoapp.app.service.order.OrderTestUtil.TAKE_IN_ORDER_DATE;
import static be.yanglin.restoapp.app.service.order.OrderTestUtil.aExistingTakeInOrder;
import static be.yanglin.restoapp.app.service.order.OrderTestUtil.aListOfDeletedTakeInOrders;
import static be.yanglin.restoapp.app.service.order.OrderTestUtil.aListOfTakeInOrders;
import static be.yanglin.restoapp.app.service.order.OrderTestUtil.aNewTakeInOrder;
import static org.junit.Assert.*;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderFacadeTest {

    @InjectMocks
    private OrderFacade service;

    @Mock
    private TakeInOrderRepository takeInOrderRepository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void getAllTakeInOrders() {
        final  List<TakeInOrder> takeInOrders = aListOfTakeInOrders(2);
        takeInOrders.addAll(aListOfDeletedTakeInOrders(2));
        when(takeInOrderRepository.findAll()).thenReturn(takeInOrders);

        final List<TakeInOrder> orders = service.getAllTakeInOrders();

        assertNotNull(orders);
        assertEquals(takeInOrders.size(), orders.size());
    }

    @Test
    public void getAllTakeInOrdersByDate() {
        final  List<TakeInOrder> takeInOrders = aListOfTakeInOrders(2);
        takeInOrders.addAll(aListOfDeletedTakeInOrders(2));

        when(takeInOrderRepository.findAllByDate(TAKE_IN_ORDER_DATE)).thenReturn(takeInOrders);

        final List<TakeInOrder> orders = service.getAllTakeInOrdersOfDate(TAKE_IN_ORDER_DATE);

        assertNotNull(orders);
        assertEquals(takeInOrders.size(), orders.size());
    }

    @Test
    public void getTakeInOrders() {
        final  List<TakeInOrder> takeInOrders = aListOfTakeInOrders(2);
        takeInOrders.addAll(aListOfDeletedTakeInOrders(2));

        when(takeInOrderRepository.findAll()).thenReturn(takeInOrders);

        final List<TakeInOrder> orders = service.getTakeInOrders();

        assertNotNull(orders);
        assertEquals(2, orders.size());
    }

    @Test
    public void getTakeInOrdersByDate() {
        final List<TakeInOrder> takeInOrders = aListOfTakeInOrders(2);
        takeInOrders.addAll(aListOfDeletedTakeInOrders(2));

        when(takeInOrderRepository.findAllByDate(TAKE_IN_ORDER_DATE)).thenReturn(takeInOrders);

        final List<TakeInOrder> orders = service.getTakeInOrdersOfDate(TAKE_IN_ORDER_DATE);

        assertNotNull(orders);
        assertEquals(2, orders.size());
    }

    @Test
    public void createTakeInOrder() throws TableAlreadyActiveException {
        final TakeInOrder newTakeInOrder = aNewTakeInOrder(1);
        final TakeInOrder existingTakeInOrder = aExistingTakeInOrder(1);
        when(takeInOrderRepository.insert(newTakeInOrder)).thenReturn(existingTakeInOrder);

        final Order order = service.createOrder(1);

        assertNotNull(order);
        assertEquals(existingTakeInOrder, order);
    }

    @Test
    public void deleteTakeInOrder() throws Exception {
        final TakeInOrder existingTakeInOrder = aExistingTakeInOrder(1);

        when(takeInOrderRepository.findById(existingTakeInOrder.getId())).thenReturn(Optional.of(existingTakeInOrder));

        service.deleteTakeInOrder(existingTakeInOrder.getId());

        verify(takeInOrderRepository).findById(existingTakeInOrder.getId());
        verify(takeInOrderRepository).save(existingTakeInOrder);
    }

    @Test
    public void deleteTakeInOrderNotFound() throws Exception {
        final TakeInOrder existingTakeInOrder = aExistingTakeInOrder(1);

        when(takeInOrderRepository.findById(existingTakeInOrder.getId())).thenReturn(Optional.empty());

        expectedException.expect(OrderNotFoundException.class);
        service.deleteTakeInOrder(existingTakeInOrder.getId());

        verify(takeInOrderRepository).findById(existingTakeInOrder.getId());
        verify(takeInOrderRepository, never()).save(existingTakeInOrder);
    }

    @Test
    public void payTakeInOrder() throws Exception {
        final TakeInOrder existingTakeInOrder = aExistingTakeInOrder(1);

        when(takeInOrderRepository.findById(existingTakeInOrder.getId())).thenReturn(Optional.of(existingTakeInOrder));

        service.updateOrderPayMethod(existingTakeInOrder.getId(), Order.PayMethod.Card, OrderType.TakeIn);

        verify(takeInOrderRepository).findById(existingTakeInOrder.getId());
        verify(takeInOrderRepository).save(existingTakeInOrder);
    }

    @Test
    public void payTakeInOrderNotFound() throws Exception {
        final TakeInOrder existingTakeInOrder = aExistingTakeInOrder(1);

        when(takeInOrderRepository.findById(existingTakeInOrder.getId())).thenReturn(Optional.empty());

        expectedException.expect(OrderNotFoundException.class);
        service.updateOrderPayMethod(existingTakeInOrder.getId(), Order.PayMethod.Card, OrderType.TakeIn);

        verify(takeInOrderRepository).findById(existingTakeInOrder.getId());
        verify(takeInOrderRepository, never()).save(existingTakeInOrder);
    }

    @Test
    public void changeTable() throws Exception {
        final int tableNrToChange = 2;
        final TakeInOrder takeInOrder = aExistingTakeInOrder(1);

        when(takeInOrderRepository.findById(takeInOrder.getId())).thenReturn(Optional.of(takeInOrder));
        when(takeInOrderRepository
                .findByDateAndTableNrAndStatus(takeInOrder.getDate(), tableNrToChange, Order.Status.ACTIVE))
                .thenReturn(Optional.empty());

        service.changeTable(takeInOrder.getId(), tableNrToChange);

        verify(takeInOrderRepository).findById(takeInOrder.getId());
        verify(takeInOrderRepository).findByDateAndTableNrAndStatus(
                takeInOrder.getDate(), tableNrToChange, Order.Status.ACTIVE);
        verify(takeInOrderRepository).save(takeInOrder);
    }

    @Test
    public void changeTableOrderNotFound() throws Exception {
        final int tableNrToChange = 2;
        final String id = "test";

        when(takeInOrderRepository.findById(id)).thenReturn(Optional.empty());

        expectedException.expect(OrderNotFoundException.class);
        service.changeTable(id, tableNrToChange);

        verify(takeInOrderRepository).findById(id);
    }

    @Test
    public void changeTableAlreadyActive() throws Exception {
        final int tableNrToChange = 2;
        final TakeInOrder takeInOrder = aExistingTakeInOrder(1);
        final TakeInOrder takeInOrder2 = aExistingTakeInOrder(1);

        when(takeInOrderRepository.findById(takeInOrder.getId())).thenReturn(Optional.of(takeInOrder));
        when(takeInOrderRepository
                .findByDateAndTableNrAndStatus(takeInOrder.getDate(), tableNrToChange, Order.Status.ACTIVE))
                .thenReturn(Optional.of(takeInOrder2));

        expectedException.expect(TableAlreadyActiveException.class);
        service.changeTable(takeInOrder.getId(), tableNrToChange);

        verify(takeInOrderRepository).findById(takeInOrder.getId());
        verify(takeInOrderRepository).findByDateAndTableNrAndStatus(
                takeInOrder.getDate(), tableNrToChange, Order.Status.ACTIVE);
        verify(takeInOrderRepository, never()).save(takeInOrder);
    }
}