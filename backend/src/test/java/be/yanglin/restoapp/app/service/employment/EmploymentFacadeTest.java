package be.yanglin.restoapp.app.service.employment;

import be.yanglin.restoapp.app.exception.EmployeeNotFoundException;
import be.yanglin.restoapp.app.model.employment.Employee;
import be.yanglin.restoapp.app.model.employment.Work;
import be.yanglin.restoapp.app.restAPI.model.WorkQueryDTO;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.List;

import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.START_DATE_STRING;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.EMPLOYEE_ID;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aEmployee;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aExistWork;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aListOfEmployee;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aListOfExistWorks;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aNewWork;
import static be.yanglin.restoapp.app.service.employment.EmployeeTestUtil.aWorkQueryDTO;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmploymentFacadeTest {

    @InjectMocks
    private EmploymentFacade facade;

    @Mock
    private EmployeeService employeeService;

    @Mock
    private WorkService workService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void getAllWorksOfEmployee() {
        List<Work> works = aListOfExistWorks();

        when(workService.getAllWorks(EMPLOYEE_ID)).thenReturn(works);

        List<Work> worksActual = facade.getAllWorksOfEmployee(EMPLOYEE_ID);

        assertNotNull(worksActual);
        assertEquals(works, worksActual);
        verify(workService, times(1)).getAllWorks(EMPLOYEE_ID);
    }

    @Test
    public void getAllWorksOfEmployeeOfADay() {
        final List<Work> works = aListOfExistWorks();
        final LocalDate date = LocalDate.parse(START_DATE_STRING);

        when(workService.getAllWorks(EMPLOYEE_ID, date)).thenReturn(works);

        final List<Work> worksActual = facade.getAllWorksOfEmployeeByDay(EMPLOYEE_ID, START_DATE_STRING);

        assertNotNull(worksActual);
        assertEquals(works, worksActual);
        verify(workService, times(1)).getAllWorks(EMPLOYEE_ID, date);
    }

    @Test
    public void getAllWorksOfEmployeeOfAWeek() {
        final List<Work> works = aListOfExistWorks();
        final WorkQueryDTO workQueryDTO = aWorkQueryDTO();
        LocalDate startDate = LocalDate.parse(workQueryDTO.getStartDate());
        LocalDate endDate = LocalDate.parse(workQueryDTO.getEndDate());

        when(workService.getAllWorks(EMPLOYEE_ID, startDate, endDate)).thenReturn(works);

        final List<Work> worksActual = facade.getWorksOfEmployeeOfWeek(workQueryDTO);

        assertNotNull(worksActual);
        assertEquals(works, worksActual);
        verify(workService, times(1)).getAllWorks(EMPLOYEE_ID, startDate, endDate);
    }

    @Test
    public void addNewWork() {
        final Work newWork = aNewWork();
        final Work work = aExistWork();

        when(workService.updateOrAddWork(newWork)).thenReturn(work);

        final Work workActual = facade.addNewWork(newWork);

        assertNotNull(workActual);
        assertEquals(work, workActual);
        verify(workService, times(1)).updateOrAddWork(newWork);
    }

    @Test
    public void getAllEmployees() {
        List<Employee> employees = aListOfEmployee();

        when(employeeService.getAllEmployees()).thenReturn(employees);

        List<Employee> employeesActual = facade.getAllEmployee();

        assertNotNull(employeesActual);
        assertEquals(employees, employeesActual);
        verify(employeeService, times(1)).getAllEmployees();
    }

    @Test
    public void getEmployeeById() throws Exception {
        final Employee employeeExpected = aEmployee();
        when(employeeService.getEmployeeById("test")).thenReturn(employeeExpected);

        Employee employee = facade.getEmployeeById("test");

        assertNotNull(employee);
        assertEquals(employeeExpected, employee);
    }

    @Test
    public void getEmployeeByIdWhenNotFound() throws Exception {
        when(employeeService.getEmployeeById("notFound")).thenThrow(EmployeeNotFoundException.class);

        expectedException.expect(EmployeeNotFoundException.class);
        facade.getEmployeeById("notFound");
    }

    @Test
    public void getEmployeeByName() throws Exception {
        final Employee employee = aEmployee();
        when(employeeService.getEmployeeByName(employee.getFirstName(), employee.getLastName()))
                .thenReturn(employee);

        final Employee employeeActual = facade.getEmployeeByName(employee.getFirstName(), employee.getLastName());

        assertNotNull(employeeActual);
        assertEquals(employee, employeeActual);
        verify(employeeService, times(1))
                .getEmployeeByName(employee.getFirstName(), employee.getLastName());
    }

    @Test
    public void addEmployee() {
        final Employee employee = aEmployee();

        when(employeeService.addNewEmployee(employee)).thenReturn(employee);

        final Employee employeeActual = facade.addNewEmployee(employee);

        assertNotNull(employeeActual);
        assertEquals(employee, employeeActual);
        verify(employeeService, times(1)).addNewEmployee(employee);
    }

    @Test
    public void updateEmployee() {
        final  Employee employee = aEmployee();

        when(employeeService.updateEmployee(employee)).thenReturn(employee);

        final Employee employeeActual = facade.updateEmployee(employee);

        assertNotNull(employeeActual);
        assertEquals(employee, employeeActual);
        verify(employeeService, times(1)).updateEmployee(employee);
    }

}