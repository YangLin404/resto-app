package be.yanglin.restoapp.app.service.product;

import be.yanglin.restoapp.app.exception.ProductNotUniqueException;
import be.yanglin.restoapp.app.model.product.Product;
import be.yanglin.restoapp.app.model.product.ProductType;
import be.yanglin.restoapp.app.repo.product.ProductRepository;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    private final static List<Product> PRODUCT_LIST = new ArrayList<>();

    @InjectMocks
    private ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @BeforeClass
    public static void initTest() {
        Arrays.stream(ProductType.values()).forEach(pt ->
                PRODUCT_LIST.add(new Product("test_" + pt, "test", "ook test", 22, pt)));
    }

    @Test
    public void getAllProduct() {
        when(productRepository.findAll()).thenReturn(PRODUCT_LIST);

        List<Product> productsActual = productService.getAllProduct();

        assertEquals(productsActual,PRODUCT_LIST);
        verify(productRepository,times(1)).findAll();
    }

    @Test
    public void getAllProductTypes() {
        List<ProductType> types = productService.getAllProductTypes();

        assertEquals(Arrays.asList(ProductType.values()), types);
    }

    @Test
    public void getProductById() {
        final Product product = aProduct();
        when(productRepository.findById("test")).thenReturn(Optional.of(product));

        Product productActual = productService.getProductById("test");

        assertNotNull(productActual);
        assertEquals(product, productActual);
    }

    @Test
    public void updateProduct() throws Exception {
        final Product product = aProduct();
        when(productRepository.save(product)).thenReturn(product);

        Product productActual = productService.updateOrAddNewProduct(product);

        assertNotNull(productActual);
        assertNotNull(productActual.getId());
        assertNotEquals("", productActual.getId());
        assertEquals(product, productActual);
        verify(productRepository, times(1)).save(product);
    }

    @Test
    public void addNewProduct() throws Exception {
        final Product product = aNewProduct();
        final Product productExpected = aProduct();

        when(productRepository.save(product)).thenReturn(productExpected);

        Product productActual = productService.updateOrAddNewProduct(product);

        assertNotNull(productActual);
        assertNotNull(productActual.getId());
        assertEquals(productExpected, productActual);
        assertNotEquals("", productActual.getId());
        verify(productRepository, times(1)).save(product);
    }

    @Test(expected = ProductNotUniqueException.class)
    public void addNewProductWithDuplicatedQuicklink() throws Exception {
        final Product product = aNewProduct();

        when(productRepository.existsByQuicklink(product.getQuicklink())).thenReturn(true);

        productService.updateOrAddNewProduct(product);

        verify(productRepository, never()).save(product);
        verify(productRepository, times(1)).existsByQuicklink(product.getQuicklink());
    }

    @Test
    public void deleteProduct() {
        productService.deleteProduct("testId");

        verify(productRepository, times(1)).deleteById("testId");
    }

    @Test
    public void isANewProduct() {
        final Product product = aNewProduct();

        boolean isANewProduct = productService.isANewProduct(product);

        assertEquals(true, isANewProduct);
    }

    @Test
    public void isNotANewProduct() {
        final Product product = aProduct();

        boolean isANewProduct = productService.isANewProduct(product);

        assertEquals(false, isANewProduct);
    }

    @Test
    public void productIsUnique() {
        final Product product = aNewProduct();

        when(productRepository.existsByQuicklink(product.getQuicklink())).thenReturn(false);

        boolean isProductUnique = productService.isProductUnique(product);

        assertEquals(true, isProductUnique);
        verify(productRepository, times(1)).existsByQuicklink(product.getQuicklink());
    }

    @Test
    public void productIsNotUnique() {
        final Product product = aNewProduct();

        when(productRepository.existsByQuicklink(product.getQuicklink())).thenReturn(true);

        boolean isProductUnique = productService.isProductUnique(product);

        assertEquals(false, isProductUnique);
        verify(productRepository, times(1)).existsByQuicklink(product.getQuicklink());
    }

    @Test
    public void isProductListEmpty() {
        when(productRepository.count()).thenReturn(0L);

        boolean isProductListEmpty = productService.isProductListEmpty();

        assertEquals(true, isProductListEmpty);
        verify(productRepository, times(1)).count();
    }

    @Test
    public void productListIsNotEmpty() {
        when(productRepository.count()).thenReturn(1L);

        boolean isProductListEmpty = productService.isProductListEmpty();

        assertEquals(false, isProductListEmpty);
        verify(productRepository, times(1)).count();
    }

    private Product aProduct() {
        return new Product("testID", "test_", "testName", "ook test", 22, ProductType.MainDishe);
    }

    private Product aNewProduct() {
        return new Product("", "test_", "testName", "ook test", 22, ProductType.MainDishe);
    }

}