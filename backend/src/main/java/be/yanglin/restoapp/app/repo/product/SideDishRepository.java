package be.yanglin.restoapp.app.repo.product;

import be.yanglin.restoapp.app.model.product.SideDish;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SideDishRepository extends MongoRepository<SideDish, String> {
    boolean existsByName(String name);
}
