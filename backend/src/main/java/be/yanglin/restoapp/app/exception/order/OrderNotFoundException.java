package be.yanglin.restoapp.app.exception.order;

import be.yanglin.restoapp.app.model.order.OrderType;

public class OrderNotFoundException extends Exception {

    public OrderNotFoundException(OrderType orderType, String id) {
        super(orderType + " order with id " + id + " cannot be found.");
    }
}
