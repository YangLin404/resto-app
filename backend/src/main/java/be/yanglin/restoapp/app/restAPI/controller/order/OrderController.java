package be.yanglin.restoapp.app.restAPI.controller.order;

import be.yanglin.restoapp.app.exception.order.OrderLineNotFoundException;
import be.yanglin.restoapp.app.exception.order.OrderNotFoundException;
import be.yanglin.restoapp.app.exception.order.SupplementNotAllowedException;
import be.yanglin.restoapp.app.exception.order.TableAlreadyActiveException;
import be.yanglin.restoapp.app.model.order.Order;
import be.yanglin.restoapp.app.model.order.OrderLine;
import be.yanglin.restoapp.app.model.order.OrderType;
import be.yanglin.restoapp.app.model.order.Supplement;
import be.yanglin.restoapp.app.service.order.OrderFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    @Autowired
    private OrderFacade orderFacade;

    @PutMapping
    public Order createOrder(@RequestParam(value = "table", required = false) Integer tableNr)
            throws TableAlreadyActiveException {
        return orderFacade.createOrder(tableNr);
    }

    @PostMapping("/{id}/pay/{method}")
    public void updatePayMethod(@PathVariable("id") String id,
                                @PathVariable("method")Order.PayMethod payMethod,
                                @RequestHeader("orderType") OrderType type)
            throws OrderNotFoundException {
        orderFacade.updateOrderPayMethod(id, payMethod, type);
    }

    @PutMapping("/{id}/orderLine")
    public Order addOrderLineToOrder(@PathVariable("id") String id,
                                     @RequestHeader("orderType") OrderType orderType,
                                     @RequestBody OrderLine orderLine)
            throws OrderNotFoundException {
        return orderFacade.addOrderLine(orderLine, id, orderType);
    }

    @PutMapping("/{id}/orderLine/{productId}/supplement")
    public void addSuppToOrderLine(@PathVariable("id") String id,
                                    @PathVariable("productId") String productId,
                                    @RequestHeader("orderType") OrderType orderType,
                                    @RequestBody Supplement supplement)
            throws OrderNotFoundException, OrderLineNotFoundException, SupplementNotAllowedException {
        orderFacade.addSupToOrderLine(id, productId, supplement, orderType);

    }

    @DeleteMapping("/{id}/orderLine/{orderLineId}")
    public Order removeOrderLineFromOrder(@PathVariable("id") String id,
                                          @RequestHeader("orderType") OrderType orderType,
                                          @PathVariable("orderLineId") String orderLineId)
            throws OrderNotFoundException, OrderLineNotFoundException {
        return orderFacade.deleteOrderLineFromOrder(id, orderLineId, orderType);
    }

    @DeleteMapping("/{id}")
    public void removeTakeInOrder(@PathVariable("id") String id) throws OrderNotFoundException {
        orderFacade.deleteTakeInOrder(id);
    }
}
