package be.yanglin.restoapp.app.model.order;

import be.yanglin.restoapp.app.model.product.Product;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@ToString
public class OrderLine {

    private Product product;
    private List<Supplement> supplements;
    private String remark;

    private int count;

    public String getProductId() {
        return this.product.getId();
    }

    public void addCount() {
        this.count++;
    }

    public void minCount() { this.count--; }

    @JsonIgnore
    public boolean isSupplementFull() {
        return supplements.size() >= count;
    }

    public void addSupplement(Supplement supplement) {
        this.getSupplements().add(supplement);
    }
}
