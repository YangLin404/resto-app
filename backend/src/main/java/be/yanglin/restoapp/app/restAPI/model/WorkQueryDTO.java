package be.yanglin.restoapp.app.restAPI.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class WorkQueryDTO {

    private String employeeId;
    private String startDate;
    private String endDate;

}
