package be.yanglin.restoapp.app.exception.order;

import be.yanglin.restoapp.app.model.order.Supplement;
import be.yanglin.restoapp.app.model.product.ProductType;

public class SupplementNotAllowedException extends Exception {
    public SupplementNotAllowedException(ProductType type, Supplement supplement) {
        super("Supplement: " + supplement.getName() + " is not allowed for product type: " + type);
    }
}
