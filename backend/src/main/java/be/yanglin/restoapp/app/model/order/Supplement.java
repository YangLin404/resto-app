package be.yanglin.restoapp.app.model.order;

import be.yanglin.restoapp.app.model.product.SideDish;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@EqualsAndHashCode
public class Supplement {

    @Id
    public String id;

    private String name;

    private double price;

    public Supplement(SideDish sideDish) {
        this.name = sideDish.getName();
        this.price = sideDish.getPrice();
    }
}
