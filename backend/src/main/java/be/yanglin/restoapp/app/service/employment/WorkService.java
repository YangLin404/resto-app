package be.yanglin.restoapp.app.service.employment;

import be.yanglin.restoapp.app.model.employment.Employee;
import be.yanglin.restoapp.app.model.employment.Work;
import be.yanglin.restoapp.app.repo.WorkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.List;

@Service
public class WorkService {

    @Autowired
    private WorkRepository workRepository;

    public List<Work> getAllWorks(String employeeId, LocalDate day) {
        return workRepository.findAllByEmployeeIdAndDay(employeeId, day);
    }

    public List<Work> getAllWorks(String employeeId) {
        return workRepository.findAllByEmployeeId(employeeId);
    }

    public Work updateOrAddWork(Work work) {
        if (work.getId().isEmpty()) {
            work.setId(null);
            return workRepository.insert(work);
        }
        return workRepository.save(work);
    }

    public List<Work> updateWorks(List<Work> works) {
        return workRepository.saveAll(works);
    }

    public void deleteWork(Work work) {
        workRepository.delete(work);
    }

    public List<Work> getAllWorks(String employeeId, LocalDate startDate, LocalDate endDate)
            throws DateTimeException {
        List<Work> works;
        if (startDate.isAfter(endDate)) {
            throw new DateTimeException("start date cannot be after than end date");
        } else if (startDate.isEqual(endDate)) {
            works = getAllWorks(employeeId, startDate);
        } else {
            works = workRepository.findByEmployeeIdAndDayBetween(employeeId, startDate, endDate);
        }
        return works;
    }
}
