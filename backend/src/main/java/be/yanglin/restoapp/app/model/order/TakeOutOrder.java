package be.yanglin.restoapp.app.model.order;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
@Document
public class TakeOutOrder extends Order {

    private boolean isTaken;
    private String name;

    public TakeOutOrder() {
        this.setOrderType(OrderType.TakeOut);
    }
}
