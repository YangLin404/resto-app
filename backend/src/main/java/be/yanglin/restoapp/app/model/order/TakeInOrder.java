package be.yanglin.restoapp.app.model.order;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Document
public class TakeInOrder extends Order {

    private int tableNr;

    @Builder
    public TakeInOrder(LocalDate date, LocalTime time, Status status, PayMethod payMethod, List<OrderLine> orderLines, int tableNr) {
        super.setDate(date);
        super.setTime(time);
        super.setStatus(status);
        super.setPayMethod(payMethod);
        super.setOrderType(OrderType.TakeIn);
        super.setOrderLines(orderLines);
        this.tableNr = tableNr;
    }

    public static TakeInOrder createNewOrder(int tableNr) {
        return TakeInOrder.builder()
                .date(LocalDate.now())
                .time(LocalTime.now())
                .orderLines(new ArrayList<>())
                .payMethod(Order.PayMethod.None)
                .tableNr(tableNr)
                .status(Order.Status.ACTIVE)
                .build();
    }
}
