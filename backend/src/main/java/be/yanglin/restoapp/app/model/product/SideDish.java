package be.yanglin.restoapp.app.model.product;

import lombok.*;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
public class SideDish {

    @Id
    public String id;

    private String name;

    private double price;
}
