package be.yanglin.restoapp.app.restAPI.controller.product;

import be.yanglin.restoapp.app.exception.ProductNotUniqueException;
import be.yanglin.restoapp.app.model.product.Product;
import be.yanglin.restoapp.app.model.product.ProductType;
import be.yanglin.restoapp.app.service.product.ProductService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    private final static Logger LOGGER = LogManager.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    @GetMapping
    public List<Product> getProducts() {
        LOGGER.debug("get all products called");
        return productService.getAllProduct();
    }

    @GetMapping(value = "/productType")
    public List<ProductType> getProductTypes() {
        LOGGER.debug("get all producttypes called");
        return productService.getAllProductTypes();
    }

    @GetMapping(value = "/{id}")
    public Product getProductById(@PathVariable String id) {
        LOGGER.debug("get product with id: " + id + " called");
        Product p = productService.getProductById(id);
        LOGGER.debug("returning product: " + p.toString());
        return p;
    }

    @PostMapping
    public Product updateOrAddNewProduct(@RequestBody Product product) throws ProductNotUniqueException {
        LOGGER.debug("update or add new product called product to update: " + product.toString());
        Product p = productService.updateOrAddNewProduct(product);
        LOGGER.debug("product after updating or addition: " + p.toString());
        return p;
    }

    @DeleteMapping(value = "/{id}")
    public void deleteProduct(@PathVariable String id) {
        LOGGER.debug("delete product with id: " + id + " called.");
        this.productService.deleteProduct(id);
    }
}
