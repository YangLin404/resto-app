package be.yanglin.restoapp.app.repo;

import be.yanglin.restoapp.app.model.employment.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface EmployeeRepository extends MongoRepository<Employee, String> {

    Optional<Employee> findByFirstNameAndLastName(String firstName, String lastName);
}
