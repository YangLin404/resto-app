package be.yanglin.restoapp.app.repo.product;

import be.yanglin.restoapp.app.model.product.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends MongoRepository<Product, String> {

    boolean existsByQuicklink(String quicklink);

    Optional<Product> findByQuicklink(String quicklink);

    void deleteByQuicklink(String quicklink);
}
