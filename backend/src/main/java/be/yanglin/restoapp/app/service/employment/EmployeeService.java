package be.yanglin.restoapp.app.service.employment;

import be.yanglin.restoapp.app.exception.EmployeeNotFoundException;
import be.yanglin.restoapp.app.model.employment.Employee;
import be.yanglin.restoapp.app.repo.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    List<Employee> getAllEmployees() {
        return repository.findAll();
    }


    Employee getEmployeeById(String id) throws EmployeeNotFoundException {
        Optional<Employee> employeeOptional = repository.findById(id);
        if (employeeOptional.isPresent()) {
            return employeeOptional.get();
        } else {
            throw new EmployeeNotFoundException(id);
        }
    }

    Employee getEmployeeByName(String firstName, String lastName) throws EmployeeNotFoundException {
        Optional<Employee> employeeOptional = repository.findByFirstNameAndLastName(firstName, lastName);
        if (employeeOptional.isPresent()) {
            return  employeeOptional.get();
        } else {
            throw new EmployeeNotFoundException(firstName, lastName);
        }
    }

    public Employee addNewEmployee(Employee newEmployee) {
        return repository.insert(newEmployee);
    }

    public Employee updateEmployee(Employee employee) {
        return repository.save(employee);
    }
}
