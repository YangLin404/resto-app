package be.yanglin.restoapp.app.restAPI.controller.product;

import be.yanglin.restoapp.app.exception.SideDishNotNewException;
import be.yanglin.restoapp.app.exception.SideDishNotUniqueException;
import be.yanglin.restoapp.app.model.product.SideDish;
import be.yanglin.restoapp.app.service.product.SideDishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/sideDish")
public class SideDishController {

    @Autowired
    private SideDishService sideDishService;

    @GetMapping
    public List<SideDish> getSideDishes() {
        return sideDishService.getAll();
    }

    @PutMapping
    public SideDish addNewSideDish(@RequestBody SideDish sideDish)
            throws SideDishNotUniqueException, SideDishNotNewException {
        return sideDishService.addNewSideDish(sideDish);
    }

    @DeleteMapping("/{id}")
    public void deleteSideDish(@PathVariable("id") String id) {
        sideDishService.deleteSideDish(id);
    }
}
