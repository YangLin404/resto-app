package be.yanglin.restoapp.app.model.order;

public enum OrderType {
    TakeIn, TakeOut
}
