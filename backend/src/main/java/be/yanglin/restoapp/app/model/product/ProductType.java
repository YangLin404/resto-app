package be.yanglin.restoapp.app.model.product;

public enum ProductType {
    Drink, MainDishe, Soup, Entree, Dessert, Supplement, Menu
}
