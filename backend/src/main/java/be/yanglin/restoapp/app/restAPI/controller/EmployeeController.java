package be.yanglin.restoapp.app.restAPI.controller;

import be.yanglin.restoapp.app.exception.EmployeeNotFoundException;
import be.yanglin.restoapp.app.model.employment.Employee;
import be.yanglin.restoapp.app.service.employment.EmploymentFacade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    private final static Logger LOGGER = LogManager.getLogger(EmployeeController.class);

    @Autowired
    private EmploymentFacade employmentFacade;

    @GetMapping
    @ResponseBody
    public List<Employee> getAllEmployees() {
        LOGGER.info("get all employee called");
        return employmentFacade.getAllEmployee();
    }

    @GetMapping(path = "/{id}")
    @ResponseBody
    public Employee getEmployeeById(@PathVariable(value = "id") String id) throws EmployeeNotFoundException {
        LOGGER.info("get employee by id: " + id);
        return employmentFacade.getEmployeeById(id);
    }

    @GetMapping(params = {"firstName", "lastName"})
    @ResponseBody
    public Employee getEmployeeByName(@RequestParam(value = "firstName") String firstName,
                                      @RequestParam(value = "lastName") String lastName)
            throws EmployeeNotFoundException {
        LOGGER.info("get employee by name: " + firstName + " " + lastName);
        return employmentFacade.getEmployeeByName(firstName, lastName);
    }

    @PutMapping
    @ResponseBody
    public Employee addEmployee(@RequestBody Employee employee) {
        LOGGER.info("try to add new employee " + employee);
        return employmentFacade.addNewEmployee(employee);
    }

    @PostMapping
    @ResponseBody
    public Employee updateEmployee(@RequestBody Employee employee) {
        LOGGER.info("try to update employee " + employee);
        return employmentFacade.updateEmployee(employee);
    }

}
