package be.yanglin.restoapp.app.service.order;

import be.yanglin.restoapp.app.exception.order.OrderLineNotFoundException;
import be.yanglin.restoapp.app.exception.order.OrderNotFoundException;
import be.yanglin.restoapp.app.exception.order.SupplementNotAllowedException;
import be.yanglin.restoapp.app.exception.order.TableAlreadyActiveException;
import be.yanglin.restoapp.app.model.order.Order;
import be.yanglin.restoapp.app.model.order.OrderLine;
import be.yanglin.restoapp.app.model.order.OrderType;
import be.yanglin.restoapp.app.model.order.Supplement;
import be.yanglin.restoapp.app.model.order.TakeInOrder;
import be.yanglin.restoapp.app.model.order.TakeOutOrder;
import be.yanglin.restoapp.app.model.product.ProductType;
import be.yanglin.restoapp.app.repo.order.TakeInOrderRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderFacade {

    @Autowired
    private TakeInOrderRepository takeInOrderRepository;

    private final static Logger LOGGER = LogManager.getLogger(OrderFacade.class);

    public List<TakeInOrder> getAllTakeInOrders() {
        return takeInOrderRepository.findAll();
    }

    public Order createOrder(Integer tableNr) throws TableAlreadyActiveException {
        if (tableNr != null) {
            LOGGER.info("creating new Take in order for table : " + tableNr);
            if (isTableAvailable(tableNr, LocalDate.now())) {
                throw new TableAlreadyActiveException(tableNr);
            }
            TakeInOrder newOrder = TakeInOrder.createNewOrder(tableNr);
            return takeInOrderRepository.insert(newOrder);
        } else {
            return null;
        }
    }

    public Order addOrderLine(OrderLine orderLine, String orderId, OrderType type)
            throws OrderNotFoundException {
        LOGGER.info("add orderline: " + orderLine.getProduct().getName() + " to order: " + orderId);
        Order order = findOrderById(orderId, type);
        addOrderLineToOrder(orderLine, order);
        return saveOrder(order, type);
    }

    public void addSupToOrderLine(String orderId, String productId, Supplement supplement, OrderType type)
            throws OrderNotFoundException, OrderLineNotFoundException, SupplementNotAllowedException {
        LOGGER.info("add supplement: " + supplement + " to orderline: " + productId + "of order: " + orderId);
        Order order = type == OrderType.TakeIn ? findTakeInOrder(orderId) : findTakeInOrder(orderId);
        OrderLine orderLineFound = findOrderLineByProductId(orderId ,productId, order.getOrderLines());
        if (orderLineFound.getProduct().getType() != ProductType.MainDishe) {
            throw new SupplementNotAllowedException(orderLineFound.getProduct().getType(), supplement);
        }
        if (orderLineFound.isSupplementFull()) {
            replaceLastSupOfOrderLine(orderLineFound, supplement);
        } else {
            orderLineFound.addSupplement(supplement);
        }

    }

    private Order findOrderById(String orderId, OrderType type) throws OrderNotFoundException {
        return type == OrderType.TakeIn ? findTakeInOrder(orderId) : findTakeOutOrder(orderId);
    }

    private OrderLine findOrderLineByProductId(String orderId ,String productId, List<OrderLine> orderLines)
            throws OrderLineNotFoundException {
        return orderLines.stream()
                .filter(o -> o.getProductId().equals(productId))
                .findAny()
                .orElseThrow(() -> new OrderLineNotFoundException(orderId, productId));
    }

    public List<TakeInOrder> getTakeInOrders() {
        return filterDeletedTakeInOrders(takeInOrderRepository.findAll());
    }

    public List<TakeInOrder> getTakeInOrdersOfDate(LocalDate date) {
        return filterDeletedTakeInOrders(takeInOrderRepository.findAllByDate(date));
    }

    public List<TakeInOrder> getAllTakeInOrdersOfDate(LocalDate date) {
        return takeInOrderRepository.findAllByDate(date);
    }

    public void deleteTakeInOrder(String id) throws OrderNotFoundException {
        LOGGER.info("remove take in order: " + id);
        TakeInOrder takeInOrder = findTakeInOrder(id);

        takeInOrder.setStatus(Order.Status.DELETED);
        takeInOrderRepository.save(takeInOrder);
    }

    public Order deleteOrderLineFromOrder(String orderId, String productId, OrderType orderType)
            throws OrderNotFoundException, OrderLineNotFoundException {
        LOGGER.info("remove product: " + productId + " from order: " + orderId);
        Order order = findOrderById(orderId, orderType);
        OrderLine orderLineFound = findOrderLineByProductId(orderId ,productId, order.getOrderLines());
        if (orderLineFound.getCount() > 1) {
            orderLineFound.minCount();
        } else {
            order.getOrderLines().remove(orderLineFound);
        }
        return saveOrder(order, orderType);
    }

    public void updateOrderPayMethod(String id, Order.PayMethod payMethod, OrderType type)
            throws OrderNotFoundException {
        LOGGER.info("updating paymethod of order: " + id + " to " + payMethod );
        Order order = findOrderById(id, type);

        if (payMethod == Order.PayMethod.None) {
            order.setStatus(Order.Status.ACTIVE);
        } else {
            order.setStatus(Order.Status.PAID);
        }
        order.setPayMethod(payMethod);

        saveOrder(order, type);
        LOGGER.info("update paymethod of take in order: " + id + " to " + payMethod + " is finished" );
    }

    void changeTable(String id, int tableNr) throws OrderNotFoundException, TableAlreadyActiveException {
        TakeInOrder takeInOrder = findTakeInOrder(id);
        if (isTableAvailable(tableNr, takeInOrder.getDate())) {
            throw new TableAlreadyActiveException(tableNr);
        }

        takeInOrder.setTableNr(tableNr);
        takeInOrderRepository.save(takeInOrder);
    }

    public boolean existTakeInOrder(String id) {
        return takeInOrderRepository.existsById(id);
    }

    public boolean isTakeInOrderEmpty() {
        return takeInOrderRepository.count() == 0;
    }

    public TakeInOrder findTakeInOrder(String id) throws OrderNotFoundException {
        Optional<TakeInOrder> takeInOrderOptional = takeInOrderRepository.findById(id);

        return takeInOrderOptional
                .orElseThrow(() -> new OrderNotFoundException(OrderType.TakeIn, id));
    }

    public TakeOutOrder findTakeOutOrder(String id) {
        return null;
    }

    private Order saveOrder(Order order, OrderType orderType) {
        return orderType == OrderType.TakeIn ? takeInOrderRepository.save((TakeInOrder) order) : null;
    }

    private List<TakeInOrder> filterDeletedTakeInOrders(List<TakeInOrder> takeInOrders) {
        return takeInOrders.stream()
                .filter(o -> o.getStatus() != Order.Status.DELETED)
                .collect(Collectors.toList());
    }

    private boolean isTableAvailable(int tableNr, LocalDate date) {
        return takeInOrderRepository.findByDateAndTableNrAndStatus(
                date, tableNr, Order.Status.ACTIVE).isPresent();
    }

    private void addOrderLineToOrder(OrderLine orderLine, Order order) {
        try {
            OrderLine orderLineFound =
                    findOrderLineByProductId(order.getId(), orderLine.getProductId(), order.getOrderLines());
            LOGGER.debug("product: " + orderLineFound.getProduct().getName() + " exists in this order, add count.");
                orderLineFound.addCount();
            } catch (OrderLineNotFoundException e) {
                order.getOrderLines().add(orderLine);
        }
    }

    private void replaceLastSupOfOrderLine(OrderLine orderLine, Supplement supplement) {
        orderLine.getSupplements().set(orderLine.getSupplements().size() - 1, supplement);
    }
}
