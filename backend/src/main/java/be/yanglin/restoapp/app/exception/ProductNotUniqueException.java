package be.yanglin.restoapp.app.exception;

public class ProductNotUniqueException extends Exception {

    public ProductNotUniqueException() {
        super("product is not unique.");
    }
}
