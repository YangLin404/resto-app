package be.yanglin.restoapp.app.restAPI.controller;

import be.yanglin.restoapp.app.model.employment.Work;
import be.yanglin.restoapp.app.restAPI.model.WorkQueryDTO;
import be.yanglin.restoapp.app.service.employment.EmploymentFacade;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/work")
public class WorkController {

    private final static Logger LOGGER = LogManager.getLogger(WorkController.class);

    @Autowired
    private EmploymentFacade employmentFacade;

    @GetMapping(params = "employeeId")
    @ResponseBody
    public List<Work> getAllWorksOfEmployee(@RequestParam(value = "employeeId") String employeeId) {
        LOGGER.info("get all works of employee with id: " + employeeId);
        return employmentFacade.getAllWorksOfEmployee(employeeId);
    }

    @GetMapping(params = {"employeeId", "day"})
    @ResponseBody
    public List<Work> getAllWorksOfEmployeeByDay(@RequestParam(value = "employeeId") String employeeId,
                                                 @RequestParam(value = "day") String day) {
        LOGGER.info("get all works of employee with id: " + employeeId + " by day: " + day);
        return employmentFacade.getAllWorksOfEmployeeByDay(employeeId, day);
    }

    @GetMapping
    @ResponseBody
    public List<Work> getAllWorksOfEmployeeByWeek(@RequestBody WorkQueryDTO workQueryDTO) {
        LOGGER.info("get all works of employee by week: " + workQueryDTO.toString());
        return employmentFacade.getWorksOfEmployeeOfWeek(workQueryDTO);
    }

    @PutMapping
    @ResponseBody
    public Work addNewWork(@RequestBody Work work) {
        LOGGER.info("add new work: " + work.toString());
        return employmentFacade.addNewWork(work);
    }
}
