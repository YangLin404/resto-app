package be.yanglin.restoapp.app.model.order;

import lombok.*;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public abstract class Order {

    @Id
    public String id;

    private LocalDate date;

    private LocalTime time;

    private Status status;

    private PayMethod payMethod;

    private OrderType orderType;

    private List<OrderLine> orderLines;

    public enum Status{
        ACTIVE, PAID, DELETED
    }

    public enum PayMethod {
        Cash, Card, None
    }
}
