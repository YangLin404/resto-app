package be.yanglin.restoapp.app.model.product;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@Document(collection = "products")
public class Product {

    @Id
    private String id;

    private String quicklink;
    private String name;
    private String ch_name;
    private double price;
    private ProductType type;

    public Product() {
        this.id = null;
    }

    public Product(String quicklink, String name, String ch_name, double price, ProductType type) {
        this();
        this.quicklink = quicklink;
        this.name = name;
        this.ch_name = ch_name;
        this.price = price;
        this.type = type;
    }

    public Product(String id ,String quicklink, String name, String ch_name, double price, ProductType type) {
        this(quicklink, name, ch_name, price, type);
        this.id = id;
    }
}
