package be.yanglin.restoapp.auth.controller;

import be.yanglin.restoapp.auth.model.ApplicationUser;
import be.yanglin.restoapp.auth.service.ApplicationUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/config/user")
public class UserController {

    @Autowired
    private ApplicationUserService userService;

    @PostMapping
    @ResponseBody
    public boolean addUser(@RequestBody ApplicationUser user) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        authentication.getDetails();

        return userService.addUser(user) != null;
    }

    @GetMapping
    @ResponseBody
    public List<ApplicationUser> getAllUsers() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();


        return userService.getAllUsers();
    }


}
