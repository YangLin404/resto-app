package be.yanglin.restoapp.auth.model;

public enum Role {
    ROLE_ADMIN, ROLE_USER
}
