package be.yanglin.restoapp.auth.service;

import be.yanglin.restoapp.auth.model.ApplicationUser;
import be.yanglin.restoapp.auth.repo.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApplicationUserService {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public ApplicationUser addUser(ApplicationUser user) {

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return applicationUserRepository.save(user);
    }

    public List<ApplicationUser> getAllUsers() {
        return applicationUserRepository.findAll();
    }

    public ApplicationUser getUserByUsername(String username) {
        return applicationUserRepository.findByUsername(username);
    }

    public boolean doesUserExist(String username) {
        return applicationUserRepository.existsByUsername(username);
    }
}
